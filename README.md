# About kgraben.com

This project was built to create a portfolio for myself and give people a place to go to view my projects and my development capabilities. I am constantly adding improvements and content to this site. If you are interested in having me work for your or on a project you have in mind, contact me at [my website](http://www.kgraben.com).
