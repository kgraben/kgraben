import React, {Component} from "react";
import {motion} from "framer-motion";
import Button from "react-bootstrap/Button";
import "./Resume.scss";
export default class Resume extends Component {
  componentDidMount = () =>{document.title = "kgraben - Resume";}
  downloadResume = (e) => {
      window.open("./assets/files/kurtis_graben_resume.pdf", "_blank");
  }
  render(){
    const content = {
        animate: {
          transition: { staggerChildren: 0.1 },
        },
      };
      const title = {
        initial: { y: -20, opacity: 0 },
        animate: {
          y: 0,
          opacity: 1,
          transition: {
            duration: 0.7,
            ease: [0.6, -0.05, 0.01, 0.99],
          },
        },
      };
    return (
        <motion.section exit={{ opacity: 0 }}>
            <motion.div variants={content} animate="animate" initial="initial">
                <motion.div variants={title}>
                    <div itemscope itemtype="https://schema.org/Person">
                        
                        <Button variant="outline-primary" className="no-print" onClick={this.downloadResume}>Click here to download my resume</Button>
                        <hr className="no-print"/>
                        <div className="resume-section" id="header">
                            <div className="center">
                                <div className="image">
                                    <img src="./assets/files/kurtis.jpg" className="resume-img" alt="Kurtis" />
                                </div>
                                <h1 itemProp="name">Kurtis Graben</h1>
                                <h3 itemProp="jobTitle">Full-Stack Developer</h3>
                                <div id="contact">
                                <i className="fa fa-phone" aria-hidden="true"></i>
                                    <span itemprop="telephone">&nbsp;<a href="tel:256-506-7811">(256) 506-7811</a></span>
                                    <b>&nbsp;|&nbsp;</b>
                                    <i className="fa fa-envelope-o" aria-hidden="true"></i>
                                    <span itemProp="email">&nbsp;<a href="mailto:kurtis.graben@gmail.com">kurtis.graben@gmail.com</a></span>
                                    <b>&nbsp;|&nbsp;</b>
                                    <i className="fa fa-linkedin" aria-hidden="true"></i>
                                    <span>&nbsp;<a href="https://www.linkedin.com/in/kurtisgraben/" target="_blank" rel="noreferrer">kurtisgraben</a></span>
                                </div>
                            </div>
                        </div>
                        <div className="resume-section" id="skills">
                            <h2>Skills</h2>
                            <hr/>
                            <div>
                                <span className="title">Languages</span>
                                <span className="list">C#, VBScript, Classic ASP, SQL, T-SQL, Python, Javascript, JQuery, HTML, CSS/Bootstrap</span>
                            </div>
                            <div>
                                <span className="title">Frameworks</span>
                                <span className="list">ASP.Net Webforms/MVC, React</span>
                            </div>
                            <div>
                                <span className="title">Databases</span>
                                <span className="list">SQL Server, MongoDB, PostgreSQL</span>
                            </div>
                            <div>
                                <span className="title">Systems</span>
                                <span className="list">Windows Server, Linux (Debian, Redhat), MacOS</span>
                            </div>
                        </div>
                        <div className="resume-section" id="experience">
                            <h2>Experience</h2>
                            <hr/>
                            <div id="bbva">
                                <div>
                                    <span className="company">BBVA USA</span>
                                    <span className="location">Birmingham, AL</span>
                                </div>
                                <div>
                                    <span className="title">Application Programmer Analyst (Full-stack developer)</span>
                                    <span className="date">Oct.2019 - Present</span>
                                </div>
                                <div className="responsibilities">
                                    <ul>
                                        <li>Full-stack developer on a team that develops and maintains 65+ intranet applications that touch almost every aspect of the bank including engineering, finance, risk and human resources.</li>
                                        <li>Strong focus on developing applications utilizing .NET (C#/VB), Javascript (JQuery and other applicable libraries), SQL Databases (including SQL, T-SQL, Stored Procedures), AWS cloud environments and Redhat Linux.</li>
                                        <li>Took over and finalized a project to rewrite a legacy coldfusion web application to .NET (C#) that managed high level bank director stock purchases and business development transactions.</li>
                                        <li>Developed an internal application that tracked BBVA’s impact and contribution to sustainability. This application is used internally to provide reports for government audits and regulation.</li>
                                    </ul>
                                </div>
                            </div>
                            <div id="itac">
                                <div>
                                    <span className="company">ITAC Solutions</span>
                                    <span className="location">Birmingham, AL</span>
                                </div>
                                <div>
                                    <span className="title">Application Programmer Analyst (Contract to hire with BBVA)</span>
                                    <span className="date">Apr. 2019 - Oct.2019</span>
                                </div>
                                <div className="responsibilities">
                                    <ul>
                                        <li>Updated the look and feel of 15+ internal web applications for BBVA’s new rebranding updates. This included modernizing 10-15+ year old applications to meet new security and company programming standards.</li>
                                        <li>Assisted in migrating web apps to Bitbucket and Bamboo to improve team collaboration and workflow.</li>
                                    </ul>
                                </div>
                            </div>
                            <div id="pentius">
                                <div>
                                    <span className="company">Pentius</span>
                                    <span className="location">Remote</span>
                                </div>
                                <div>
                                    <span className="title">Full-Stack Developer</span>
                                    <span className="date">Apr. 2015 - Apr. 2019</span>
                                </div>
                                <div className="responsibilities">
                                    <ul>
                                        <li>Full-stack developer on development team that develops and maintains VoIP customer relationship management software for remote and on-site call center agents utilizing Classic ASP, SQL databases (SQL, T-SQL, Stored Procedures), Python, Javascript (JQuery, React) and MongoDB</li>
                                        <li>Team lead to develop a modernized CRM dashboard utilizing asynchronous programming, real-time alerts and notifications and integrate an inter-active voice response (IVR) system.</li>
                                        <li>Developed a real-time diagnostic tool that is capable of providing technical statistics of any ongoing or past calls. This report, originally created for technical diagnostics, eventually morphed and used by managers to track agents productivity and administration.</li>
                                        <li>Built and maintained reports for all management levels ranging from CEO to call-center team leads. These reports utilized multiple search options, drill down functionality and role-based security to provide quick, simple searches.</li>
                                        <li>Designed and implemented role-based security and maintenance dashboard to allow managers grant/deny access to functionality in the customer service dashboard. This dashboard was capable of logging changes for security audit as well as toggling access on user, role and team levels that follows a ”least privilege” policy.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="resume-section" id="education">
                            <h2>Education</h2>
                            <hr/>
                            <div id="jacksonville">
                                <div>
                                    <span itemProp="alumniOf" className="institution">Jacksonville State University</span>
                                    <span className="city">Jacksonville, AL</span>
                                </div>
                                <div>
                                    <span className="degree">Bachelor of Science in Computer Information Systems</span>
                                    <span className="date">Aug. 2013 - Aug 2017</span>
                                </div>
                                <div className="accomplishments">
                                    <div>
                                        <span className="title">Concentration</span>
                                        <span>Web Development</span>
                                    </div>
                                    <div>
                                        <span className="title">Course Highlights</span>
                                        <span>Python, Java, Database, Operating Systems, Software Engineering, Computer Architecture, Networking, Computer Security</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="resume-section" id="projects">
                            <h2>Projects/Volunteer Work</h2>
                            <hr/>
                            <div className="project">
                                <div>
                                    <span className="name"><a href="https://gitlab.com/kgraben/kgraben" target="_blank" rel="noreferrer">kgraben.com (this site)</a></span>
                                    <span className="location">Birmingham, AL</span>
                                </div>
                                <div>
                                    <span className="role">Developer/Designer</span>
                                    <span className="date">Ongoing</span>
                                </div>
                                <span className="responsibilities">
                                    <ul>
                                        <li>Project to develop my portfolio website to demonstrate skills in development and design</li>
                                        <li>Technologies
                                            <ul>
                                                <li>react</li>
                                                <li>sass</li>
                                                <li>html/css</li>
                                                <li>aws hosted linux server</li>
                                                <li>nginx</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </span>
                            </div>
                            <div className="project">
                                <div>
                                    <span className="name">Website Rebuild and Update</span>
                                    <span className="location">Birmingham, AL/Railroad Park</span>
                                </div>
                                <div>
                                    <span className="role">Developer</span>
                                    <span className="date">Nov. 2020</span>
                                </div>
                                <span className="responsibilities">
                                    <ul>
                                        <li>Day long ”hackathon” volunteer event with BBVA employees to rebuild the website for Railroad Park in Birmingham, AL.</li>
                                        <li>Modernized the look and feel, made the website mobile friendly</li>
                                        <li>Migrated website to wordpress to enable the users easy access to website maintenance</li>
                                    </ul>
                                </span>
                            </div>
                        </div>
                    </div>
                </motion.div>
            </motion.div>
        </motion.section>
    )
  }
}