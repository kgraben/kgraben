import React, {Component} from "react";
import { Route, HashRouter, Switch, Redirect } from "react-router-dom";
import {AnimatePresence} from "framer-motion";
import NavBar from "./components/navbar";
import { Layout } from "./components/Layout";
import Home from "./Home";
import Resume from "./Resume";
import About from "./About";
import Projects from "./Projects";
import './App.scss';

export default class App extends Component {
  static displayName = App.name;
  render(){
    return (
      <Layout>
        <HashRouter>
          <NavBar/>
          <AnimatePresence exitBeforeEnter>
            <Switch>
              <Route exact path='/' component={Home}/>
              <Route exact path='/Resume' component={Resume}/>
              <Route exact path='/About' component={About}/>
              <Route exact path='/Projects' component={Projects}/>
              <Route path='*'>
                <Redirect to="/"/>
              </Route>
            </Switch>
          </AnimatePresence>
        </HashRouter>
      </Layout>
    )
  }
}