import React, {Component} from "react";
import {motion} from "framer-motion";
import './Project.scss';
export default class Projects extends Component {
  componentDidMount = () =>{document.title = "kgraben - Projects";}
  state = {
      DevYears: new Date().getFullYear() - 2015
  }
  render(){
    const content = {
      animate: {
        transition: { staggerChildren: 0.1 },
      },
    };
    const title = {
      initial: { y: -20, opacity: 0 },
      animate: {
        y: 0,
        opacity: 1,
        transition: {
          duration: 0.7,
          ease: [0.6, -0.05, 0.01, 0.99],
        },
      },
    };
    return (
      <motion.section exit={{ opacity: 0 }}>
        <motion.div variants={content} animate="animate" initial="initial">
          <motion.div variants={title}>
            <div>
              <div>
                <h1>Projects</h1>
                <span>Below are some projects that I have in progress or I have built as a way to demonstrate my abilities using different languages and newer technologies. These projects are ongoing and are regularly updated so if you're interested, follow the link to them on gitlab and watch. If you have any suggestions, you are more than welcome to reach out to me on gitlab to provide those.</span>
              </div>
              <div className="card-row">
                <div className="project-card">
                  <div className="project-img">
                  </div>
                  <div className="container">
                    <img src="./assets/files/kgraben.jpg" alt="kgraben website" className="card-img" />
                    <h4>kgraben.com</h4>
                    <p>This is an ongoing project for my personal portfolio site. It was built with ReactJS and is hosted in AWS.</p>
                    <a href="https://gitlab.com/kgraben/kgraben" rel="noreferrer" target="_blank" title="View project on gitlab"><i class="fab fa-gitlab"></i> kgraben.com</a>
                  </div>
                </div>
                <div className="project-card">
                  <div className="project-img">
                  </div>
                  <div className="container">
                    <img src="./assets/files/patientintake.jpg" alt="patient intake project" className="card-img" />
                    <h4>Patient Intake</h4>
                    <p>This project is built with ReactJS for the frontend, and has a dotnet core api with MongoDB for the database. I have also written a NodeJS api.</p>
                    <a href="https://gitlab.com/kgraben/patientintakeapi-dotnet" rel="noreferrer" target="_blank" title="View project on gitlab"><i class="fab fa-gitlab"></i> API In dotnet core</a><br/>
                    <a href="https://gitlab.com/kgraben/patientintakeapinodejs" rel="noreferrer" target="_blank" title="View project on gitlab"><i class="fab fa-gitlab"></i> API In NodeJS</a>
                  </div>
                </div>
                <div className="project-card">
                  <div className="project-img">
                  </div>
                  <div className="container">
                    <img src="./assets/files/basiclogin.jpg" alt="basic login project" className="card-img" />
                    <h4>Basic Login</h4>
                    <p>This project is a basic login using only html/css and javascript for the frontend, NodeJS and MySQL for the backend api.</p>
                    <a href="https://gitlab.com/kgraben/kgraben" rel="noreferrer" target="_blank" title="View project on gitlab"><i class="fab fa-gitlab"></i> Basic Login</a>
                  </div>
                </div>
              </div>
            </div>
          </motion.div>
        </motion.div>
      </motion.section>
    )
  }
}