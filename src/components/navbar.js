import React, { Component } from 'react';
import {NavLink} from 'react-router-dom';
import './NavBar.scss';
import logo from '../assets/files/kgraben_logo.png';
export default class NavBar extends Component {
    state = {
        CurrentPage: window.location.pathname
    }
    render(){
        console.log(window.location);
        return (
            <div className="Navbar">
                <div className="navbar-link-list">
                    <img src={logo} className="logo" alt="logo"/>
                    <nav>
                        <NavLink exact to="/">Home</NavLink>&nbsp;
                        <NavLink to="/About">About</NavLink>&nbsp;
                        <NavLink to="/Projects">Projects</NavLink>&nbsp;
                        <NavLink to="/Resume">Resume</NavLink>&nbsp;
                    </nav>
                </div>
            </div>
        );
    }
}