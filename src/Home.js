import React, {Component} from "react";
import {motion} from "framer-motion";
import './Home.scss';
export default class Home extends Component {
  static displayName = Home.name;
  componentDidMount = () =>{document.title = "kgraben";}

  render(){
    const content = {
      animate: {
        transition: { staggerChildren: 0.1 },
      },
    };
    const title = {
      initial: { y: -20, opacity: 0 },
      animate: {
        y: 0,
        opacity: 1,
        transition: {
          duration: 0.7,
          ease: [0.6, -0.05, 0.01, 0.99],
        },
      },
    };
    return (
      <motion.section exit={{ opacity: 0 }}>
        <motion.div variants={content} animate="animate" initial="initial">
          <motion.div variants={title}>
            <div className="intro-header">
              <div className="column-left">
                <div className="img-container">
                  <img alt="kurtis profile" src="./assets/files/kurtis.jpg" className="profile-img" />
                </div>
              </div>
              <div className="column-right">
                <p>I am a software developer with a focus in developing intuitive, accessible and highly functional software for the web.</p>
                <p>I currently work at BBVA in the USA as a full stack software developer.</p>
              </div>
            </div>
          </motion.div>
        </motion.div>
      </motion.section>
    )
  }
}