import React, {Component} from "react";
import {motion} from "framer-motion";
import './About.css';
export default class About extends Component {
  componentDidMount = () =>{document.title = "kgraben - About";}
  state = {
      DevYears: new Date().getFullYear() - 2015
  }
  render(){
    const content = {
      animate: {
        transition: { staggerChildren: 0.1 },
      },
    };
    const title = {
      initial: { y: -20, opacity: 0 },
      animate: {
        y: 0,
        opacity: 1,
        transition: {
          duration: 0.7,
          ease: [0.6, -0.05, 0.01, 0.99],
        },
      },
    };
    return (
      <motion.section exit={{ opacity: 0 }}>
        <motion.div variants={content} animate="animate" initial="initial">
          <motion.div variants={title}>
            <div>
              <div id="about-experience" className="about-card">
                <h2>My Experience</h2>
                <div className="experience-image-section">
                  <img src="./assets/files/kurtis.jpg" alt="Kurtis" />
                </div>
                <p>I am a full-stack developer with {this.state.DevYears} years of experience. In my development career, I have worked on many projects ranging from small projects to update managerial reports and perform maintenance tasks to large projects to rewrite or create new applications for corporations. I have worked solo and in small teams to large teams with stakeholders in the software development process.</p>
                <p>I have worked in many different tech environments but I have the most experience working in the microsoft .net environment involving
                  <ul className="skills-list">
                    <li>Classic ASP</li>
                    <li>VBScript</li>
                    <li>Asp.net</li>
                    <li>C#</li>
                    <li>MS SQL Server</li>
                    <li>HTML/CSS</li>
                    <li>Javascript</li>
                    <li>React</li>
                    <li>MongoDB</li>
                  </ul>
                  I am constantly taking classes on Udemy and Coursera to teach myself new skill and keep up with the latest development trends, languages and tools.
                </p>
              </div>
              <div id="about-personal" className="about-card">
                <h2>Personal Life</h2>
                <div className="personal-image-section">
                  <img src="./assets/files/k_c_chicago.jpg" alt="kurtis in chicago"/>
                </div>
                <p>When I am not working or catching up in the ever evolving tech world, I spend time either at home working on cars, travelling with my wife or trying my hand with different hobbies. I also love playing guitar and playing drums, although I mostly just learn new songs and try to keep up my talent.</p>
              </div>
            </div>
          </motion.div>
        </motion.div>
      </motion.section>
    )
  }
}